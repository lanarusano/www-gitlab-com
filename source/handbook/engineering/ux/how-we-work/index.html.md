---
layout: markdown_page
title: "UX Department"
---

### On this page

{:.no_toc}

- TOC
{:toc}

# How the UX department works

The UX Department works alongside the community, Product Managers (PMs), Frontend engineers (FE), and Backend engineers (BE). PMs are responsible for kicking-off initiatives, taking action, and setting the direction of the product. PMs don't own the product; they gather feedback and give GitLab team members and the wider community space to suggest and create.

UX should assist in driving the [product vision](/direction/product-vision/) early in the process. We inform the vision by conducting generative research, as well as facilitating discussion with community members, customers, PM, FE, and BE. We are **elevated** above just the transactional workflow and **generative** in creating work rather than just executing tasks.

## Everyone can contribute

The UX department is not solely responsible for the user experience of GitLab. Everyone is encouraged to contribute their thoughts on how we can make GitLab better by opening an issue. You can use just words or include images. These images can take a variety of forms; here are just a few examples:

* Drawings or sketches to convey your idea
* Wireframes made using a software of your choosing (Balsamiq, Sketch, etc.)
* High-fidelity mockups made using the [Pajamas Design System][pajamas]
* Screenshots of changes to our UI, made by manipulating the DOM in the browser

If you are creating high-fidelity designs, please make sure to let others know that this is a proposal and needs UX review. You can ping anyone on the UX team for assistance.

## Proactive and reactive UX
We'll always have responsibility for reactive tasks that help "keep the lights on," such as bug fixes and minor UX enhancements. But we also must carve out some portion of our time for proactive work that identifies existing pain points, redefines how we approach problems, and uncovers actionable innovation and improvement opportunities. This proactive UX work enables us to create the most complete, competitive, and resilient solutions. 

It isn’t always easy to find the space for proactive UX work, but the challenge is worth it, because it's how we create a best-in-class product experience. And it's not just the UX team's responsibility; it requires a coordinated effort from Leadership, Product, and Engineering, too.

We're currently working to find the right balance between proactive and reactive UX work. While we don't have a prescriptive ratio, we also don't allot 100% of our available work time to reactive efforts. Before and at the start of each milestone, UXers should work with their managers to define the appropriate ratio, based on our active OKRs and stage group requirements. When there are questions about priority or scope, or when a UXer is concerned about meeting a deadline, they should immediately reach out to their manager to help them resolve any concerns. Comunicate early and often!

### Expectations
Every UXer: 
* Make sure discipline peers are aware of proactive UX work and what we need from them to succeed.
* Let your manager know right away, if you believe you're not trending to complete your work. The sooner your manager knows, the higher the likelihood they can resolve the issue and manage expectations.
* Account for time off (both yours and others) as best you can.
* Work efficiently. Lean UX and design reuse allow us to do more with less. Ask yourself, "What's the least we can do to solve and implement this issue *well*?" Start lo-fi, and only increase fidelity as design confidence grows and implementation requires. Leverage the design system, and cross-share learnings and assets.

Managers:
* Set a baseline capacity for proactive/reactive work each milestone; for example, 30%/70% respectively. Teams can use this information to balance work and manage expectations. 
* Quantify strategy work with clear time-to-complete (TTC) expectations, measurable goals, and deadlines.
* Resolve team questions, concerns, and blockers quickly.
* Make sure cross-functional partners are aware of UX OKRs and their associated dependencies.

### Examples of proactive UX
One example of proactive UX work is our [Experience Baselines and Recommendations](https://about.gitlab.com/handbook/engineering/ux/experience-baseline-recommendations) initiative. Another example is the generative research that our UX Researchers lead (while inviting cross-functional partners to participate). Yet another example is the ongoing effort to beautify our UI, both through [small, tactical changes](https://gitlab.com/groups/gitlab-org/-/epics/989) and through our [Pajamas Design System][pajamas].

**Experience Baselines and Recommendations** <br>
Designers use [Experience Baselines](https://about.gitlab.com/handbook/engineering/ux/experience-baseline-recommendations) to benchmark common user tasks. In many cases, tasks involve multiple stages of the product, giving designers visibility into how users traverse across stages. Designers follow with [Experience Recommendations](https://about.gitlab.com/handbook/engineering/ux/experience-baseline-recommendations) for how to improve the experience in upcoming milestones.

## Iteration

Here at GitLab, iteration means making the [smallest thing possible and getting it out as quickly as possible](/handbook/values/#iteration). Working like this allows us to reduce cycle time and get feedback from users faster, so we can continue to improve quickly and efficiently.

Iteration isn't just one of GitLab’s six founding values, [C.R.E.D.I.T](/handbook/values/#credit), it is one of the [foundational concepts in design thinking and user experience](https://www.interaction-design.org/literature/article/design-iteration-brings-powerful-results-so-do-it-again-designer). Planning too far ahead without real-world feedback can cause you to build something that doesn't meet user needs.

Iteration is especially vital in an open-source community. Keeping changes small and iterative makes it easy for anyone to contribute. Here are some examples of how we are embracing the power of iteration and using it to build GitLab:

* We aggressively break issues down into the smallest scope possible. If an effort is too big to be completed in one milestone, then it is too big. [Epics](/handbook/product/#how-to-use-epics) allow us to maintain a holistic view of an area, while breaking the work down into an MVC.
* We keep a list of improvement issues that are actively seeking contributions from the community. They are small in scope, allowing the community to contribute designs or code to the issues. You can learn more about it in the [Community Contributions](/handbook/engineering/ux/ux-department-workflow/#community-contributions) section of this handbook. You can also [view the list of issues that need UX work](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&label_name[]=Accepting+merge+requests&label_name[]=UX).
* You may notice that our [Design System](https://gitlab.com/gitlab-org/design.gitlab.com) has a lot of "to-do" items. Rather than try to tackle everything at once, we are gradually populating our component library and incrementally rolling those components out to production.

## Stable counterparts

Every Product Designer is aligned with a PM and is responsible for the same features their PM oversees.
Technical Writers each support multiple stage groups and at least one complete stage (up to four) with the goal of supporting only one stage per writer by the end of FY-2020 per the current hiring plan. UX Researchers support multiple PMs from across one or more stages. 

UXers work alongside PMs and engineering at each stage of the process: planning, discovery, implementation, and further iteration. The area a Product Designer or Technical Writer is responsible for is part of their title; for example, "Product Designer, Plan." You can see which area of the product each Product Designer or Technical Writer is aligned with in the [team org chart](/company/team/org-chart/).

Product Designers and Technical Writers may also serve as a "backup" for other areas of the product. This area will be listed on the [team page](/company/team/) under their title as an expertise; for example, "Plan expert." UX backups should be just that&mdash;backups. They are there to conduct UX reviews on MRs when the assigned UXer for that area is out. The UX lead for a given area should coordinate with the PM and their backup during scheduling for any work that is critical. Critical UX work is defined as any work that addresses an outage, a broken feature with no workaround, or the current workaround is unacceptable.

### Headcount planning

In the spirit of having "stable counterparts," we plan headcount as follows:

* **One Product Designer for every stage group**
    * 1:1 ratio of Product Designers to Product Managers
    * Approximately a 1:7 ratio of Product Designers to Engineers
* **One Technical Writer for up to four stage groups**
    * 1:3 ratio of Technical Writers to stage groups
    * Approximately a 1:21 ratio of Technical Writers to Engineers
* **One UX Researcher for every stage**
    * Approximately a 1:5 ratio of UX Researchers to Product Managers
    * Approximately a 1:35 ratio of UX Researchers to Engineers

## Collaboration model
Coming soon!