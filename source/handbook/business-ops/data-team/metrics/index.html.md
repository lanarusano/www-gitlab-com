---
layout: markdown_page
title: "KPI Index"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Maintenance

This page is meant to help map a [KPI](/handbook/ceo/kpis/) to where it is defined in the handbook. The Data Team is responsible for maintaining this page.

If a KPI is not defined, create an MR to propose a definition. Ask the appropriate business stakeholder to review and merge.

## Work Prioritization
In connection with our [Q2 OKRs](/company/okrs/fy20-q2/). We have made the following classifications on which metrics will be visualized in Periscope:

- P0 - Most important and expected to be completed in Q2
- P1 - High priority with expectation to be completed in Q2
- P2 - The data currently resides in a separate data system that provides for adequate visualization of the metric. We have de-prioritized these given that the metric is already being tracked effectively. The work to be performed in Q2 will be to ensure the definitions are clear and that we are reporting the actual KPI against our plan and target.
- Out of scope for data team - These KPIs are contained in systems that are either non-existent or are not planned to be supported by the data team in Q2.
- Regardless of classification all KPIs on this page are expected to have linked definitions with known quantities for Plan and Target values.

## GitLab KPIs

GitLab KPIs are duplicates of goals of the reports further down this page.
GitLab KPIs are the most important indicators of company performance.
The [GitLab KPIs Dashboard](https://app.periscopedata.com/app/gitlab/434327/).

1. [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) vs. plan > 1 (P0)
1. [Capital Consumption](/handbook/finance/operating-metrics/#capital-consumption) < Plan (P1)
1. [Sales efficiency](/handbook/sales/#sales-efficiency-ratio) > 1 (P0)
1. Pipe generated vs. plan > 1 (P0)
1. Wider community contributions per release (dependent on GitLab.com data) (P1)
1. [LTV / CAC](/handbook/finance/operating-metrics/#ltv-to-cac-ratio) ratio > 4 (P1)
1. [Candidate NPS](/handbook/hiring/metrics/#candidate-nps) >4.1 (out of scope for Q2)
1. [Hires vs. plan](/handbook/hiring/metrics/#hires-vs-plan) > 0.9  (P0)
1. [12 month team member turnover](/handbook/people-operations/people-operations-metrics/#team-member-turnover) < 16% (Bamboo) (P0)
1. [Merge Requests per release per engineer in product development](/handbook/engineering/performance-indicators/#average-mrs-dev-month) > 10 (GitLab.com) (P2)
1. [Uptime GitLab.com](/handbook/engineering/performance-indicators/#gitlab-com-availability) > 99.95% (dependent on PD) (out of scope for Q2)
1. [Active users per hosting platform](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/25237/diffs) (out of scope for Q2)
1. [Support Satisfaction](/handbook/support/#customer-satisfaction-ssat) (P2)
1. [Runway](/handbook/finance/operating-metrics/#cash-burn-average-cash-burn-and-runway) > 12 months (P1)
1. [MAUI](http://www.meltano.com/docs/roadmap.html#maui) (Meltano so not part of the GitLab Executive Team KPIs) > 10% WoW

## Sales KPIs

1. [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) vs. plan > 1 (P0)
1. [Field efficiency ratio](/handbook/sales/#field-efficiency-ratio) > 2 (P0)
1. [TCV](/handbook/sales/#total-contract-value-tcv) vs. plan > 1 (P0)
1. [ARR](/handbook/sales/#annual-recurring-revenue-arr) YoY > 190% (P0)
1. [Win rate](/handbook/sales/#win-rate) > 30% (P1)
1. [% of ramped reps at or above quota](/handbook/sales/#quota-ramp) > 0.7 (P1)
1. [Net Retention](/handbook/customer-success/vision/#measurement-and-kpis) > 2 (P0)
1. [Gross Retention](/handbook/customer-success/vision/#measurement-and-kpis) > 0.9 (P0)
1. Rep [IACV](/handbook/sales/#incremental-annual-contract-value-iacv)  per comp > 5 (P1)
1. [ProServe](/handbook/customer-success/professional-service-engineering//#long-term-profitability-targets) revenue vs. cost > 1.1 (P1)
1. [Services attach rate for strategic](/handbook/customer-success/professional-service-engineering/#Services-Attach-Rate-for-Strategic-Accounts) > 0.8 (P1)
1. [Self-serve sales ratio](/handbook/sales/#self-serve-sales-ratio) > 0.3 (P1)
1. [Licensed users](/handbook/sales/#licensed-users) (P1)
1. [ARPU](/handbook/sales/#revenue-per-licensed-user-also-known-as-arpu) (P0)
1. [New hire location factor](/handbook/people-operations/global-compensation/#location-factor) < 0.72 (P0)

## Marketing KPIs

1. Pipe generated vs. plan > 1 (P0)
1. Pipe-to-spend > 5 (P0)
1. [Marketing efficiency ratio] > 2 (P0)
1. [Sales Accepted Opportunity (SAO)](/handbook/business-ops/#criteria-for-sales-accepted-opportunity-sao) (P0)
1. [LTV / CAC ratio](/handbook/finance/operating-metrics/#ltv-to-cac-ratio) > 4 (P1)
1. Twitter mentions (out of scope for Q2)
1. Sessions on our marketing site (out of scope for Q2)
1. New users (P1)
1. Product Installations: Download, start of installation, success installation, created admin user, configured email, second user invited, 30 day active, updates (out of scope for Q2)
1. [Social response time](/handbook/marketing/community-relations/community-advocacy/#respond-to-every-community-question-about-gitlab-asked-online) (out of scope for Q2)
1. [Participants at Meetups with GitLab presentation](/handbook/marketing/community-relations/evangelist-program/#how-to-see-what-were-working-on) (out of scope for Q2)
1. [GitLab presentations given at meetups](/handbook/marketing/community-relations/evangelist-program/#how-to-see-what-were-working-on) (P1)
1. Wider community contributions per release (P1)
1. Monthly Active Contributors from the wider community (P1)
1. [New hire location factor](/handbook/people-operations/people-operations-metrics/#low-location-factor-reporting) < 0.72 (PO)
1. Pipeline coverage: 2X for current quarter, 1X for next quarter, and .5 for 2 QTRs out. (P1)

## People Operations KPIs

1. [Hires vs. plan](/handbook/hiring/metrics/#hires-vs-plan) > 0.9 (Bamboo & Sheets) (P0)
1. [Apply to Offer Accept (Days)](/handbook/hiring/metrics/#apply-to-offer-accept-days) < 30 (Greenhouse)(P0)
1. [Offer acceptance rate](/handbook/hiring/metrics/#offer-acceptance-rate)> 0.9 (Greenhouse) (P1)
1. [Candidate NPS](/handbook/hiring/metrics/#candidate-nps)>4.1 (out of scope for Q2)
1. [Average location factor](/handbook/people-operations/people-operations-metrics/#average-location-factor) < 0.65 (Bamboo) (P0)
1. [Percent of team members over compensation band](/handbook/people-operations/people-operations-metrics/#percent-over-compensation-band) = 0% (Google Sheets) (P0)
1. [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor) < 0.72 (Bamboo) (P0)
1. [12 month team member turnover](/handbook/people-operations/people-operations-metrics/#team-member-turnover) < 16% (Bamboo) (P0)
1. [12 month voluntary team member turnover](/handbook/people-operations/people-operations-metrics/#team-member-turnover) < 10% (Bamboo) (P0)
1. [Candidates Sourced versus Candidates Hired](/handbook/hiring/metrics/#candidates-sourced-vs-candidates-hired) > 0.28 (Greenhouse) (P1)
1. [Onboarding eNPS](/handbook/people-operations/people-operations-metrics/#onboarding-enps) >4 (out of scope for Q2)
1. [Diversity lifecycle: applications, recruited, interviews, offers, acceptance, retention](/handbook/hiring/metrics/#diversity-lifecycle-applications-recruited-interviewed-offers-extended-offers-accepted-and-retention) (Greenhouse) (out of scope for Q2)
1. [PeopleOps cost per employee](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/24958/diffs) (Netsuite over Bamboo) (P1)
1. [Discretionary bonus](/handbook/incentives/#discretionary-bonuses) per employee per month > 0.1 (Bamboo) (P1)

## Finance KPIs

1. [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) per [capital consumed](/handbook/finance/operating-metrics/#capital-consumption) > 2 (P0)
1. [Sales efficiency](/handbook/sales/#sales-efficiency-ratio) > 1.0 (P0)
1. [Magic number](/handbook/sales/#magic-number) > 1.1 (P0)
1. [Gross margin](/handbook/finance/financial-planning-and-analysis/#long-term-profitability-targets) > 0.85 (P0)
1. [Average days of sales outstanding](/handbook/finance/accounting/#days-sales-outstanding-dso) < 45 (P0)
1. [Average days to close](/handbook/finance/accounting/#month-end-review--close) (out of scope for Q2)
1. [Runway](/handbook/finance/operating-metrics/#cash-burn-average-cash-burn-and-runway) > 12 months (P1)
1. [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor) < 0.73 (P0)
1. [ARR by annual cohort](/handbook/sales/#arr-cohort) (P0)
1. [Reasons for churn](/handbook/customer-success/vision/#measurement-and-kpis) (P0)
1. [Reasons for net expansion](/handbook/customer-success/vision/#measurement-and-kpis) (P0)
1. [Refunds processed as % of orders](/handbook/support/workflows/services/gitlab_com/verify_subscription_plan.html#refunds-processed-as--of-orders) (P1)
1. [Capital Consumption](/handbook/finance/operating-metrics/#capital-consumption) < Plan (P1)

## Product KPIs

1. [Activation](/handbook/product/metrics/#activation) (out of scope for Q2 - Snowplow)
1. [Adoption](/handbook/product/metrics/#adoption) (out of scope for Q2 - Snowplow)
1. [Upsell](/handbook/product/metrics/#upsell) (out of scope for Q2 - Snowplow)
1. [Retention](/handbook/product/metrics/#retention) (out of scope for Q2 - Snowplow)

## Support KPIs
1. [Support Satisfaction (SSAT)](/handbook/support/#customer-satisfaction-ssat) (P2)
1. [Support cost](https://about.gitlab.com/handbook/support/) vs. [ARR](/handbook/sales/#annual-recurring-revenue-arr) (Netsuite) (P0)
1. [ARR](/handbook/sales/#annual-recurring-revenue-arr) per [support rep > $1.175M](https://about.gitlab.com/handbook/support/) (Zuora over Bamboo) (P0)
1. [Average daily tickets closed per customer support rep](https://about.gitlab.com/handbook/support/) (P1)
1. [Manager to customer support ratio < 10:1](https://about.gitlab.com/handbook/support/)
1. [New hire location factor < 0.xx](/handbook/hiring/charts/engineering-function/) (P1)

## Engineering KPIs

1. [Merge Requests per release per engineer in product development > 10 (GitLab.com) (P2)](/handbook/engineering/performance-indicators/#average-mrs-dev-month)
1. [Days to merge < 7 - (New)](/handbook/engineering/performance-indicators/#mean-time-to-merge-mttm)
1. [Uptime GitLab.com > 99.95% (out of scope for Q2 - Pingdom?)](/handbook/engineering/performance-indicators/#gitlab-com-availability)
1. [Performance GitLab.com (out of scope for Q2 - Pingdom? Pagerduty? Not sure)](/handbook/engineering/performance-indicators#gitlab-com-performance)
1. [Priority Support Service Level Agreement (SLA) (P2)](/handbook/support/#service-level-agreement-sla)
1. [Days to fix S1/2/3 security issues (GitLab.com) (P1)](/handbook/engineering/performance-indicators/#mttm-mean-time-to-mitigation-for-s1-s2-s3-security-vulnerabilities)
1. [GitLab.com infrastructure cost per MAU (Netsuite over GitLab.com) (P0)](/handbook/engineering/performance-indicators/#infrastructure-cost-per-gitlab-com-user)
1. [New hire location factor < 0.58 (P1)](/handbook/engineering/performance-indicators/#average-location-factor)
1. [Public Cloud Spend (Netsuite?) (P0)](/handbook/engineering/performance-indicators/#infrastructure-cost-vs-plan)

## Alliances KPIs

1. [Active SMAU](/handbook/alliances/#alliances-kpis) (out of scope for Q2)
1. [Active installations per hosting platform](/handbook/alliances/#alliances-kpis) (out of scope for Q2)
1. [Product Downloads](/alliances/#alliances-kpis)(out of scope for Q2)
1. [Acquisition velocity](https://about.gitlab.com/handbook/acquisitions/)
1. [Acquisition success](https://about.gitlab.com/handbook/acquisitions/)
1. [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor) < 0.82 (P0)

## GitLab Metrics

We share a spreadsheet with investors called "GitLab Metrics", these can be found in the [dashboard with the same name](https://app.periscopedata.com/app/gitlab/409920/WIP:-GitLab-Metrics).

## Satisfaction

We do satisfaction scope on a scale of 1 to 5 how satisfied people are with the experience.
We don’t use NPS since that cuts off certain scores and we want to preserve fidelity.
We have the following abbreviation letter before SAT, please don’t use SAT without letter before to specify it:

- C = customer
- U = user
- T = team-members
- A = applicant
- S = support
- E = unused since employee is used by other companies but not by us


