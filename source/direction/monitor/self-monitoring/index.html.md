---
layout: markdown_page
title: "Category Vision - GitLab Self-Monitoring"
---

- TOC
{:toc}

## GitLab Self-Monitoring
We want to provide administrators with a great out-of-the-box observability solution for monitoring their GitLab instance, reducing the initial and ongoing effort required to operate GitLab.

There are three main use cases we want to improve:
* Easily understanding the health of your instance
* Proactive notifications when attention is required, _before_ it becomes an issue
* The telemetry needed to quickly isolate a problem and restore service, and identify the root cause

## Target Audience and Experience
The target audience is primarily ~"Persona: Systems Administrator", in particular the administrators responsible with operating GitLab.

We also plan to allow administrators to expose a user facing status page, to easily communicate the health of the instance and any active incidents.

## What's Next & Why

#### MVC - Bundle Grafana

Currently we provide Prometheus out of the box, as well as a rich set of exporters and instrumentation, which are automatically scraped. However we rely on users to then connect the Prometheus instance to a dashboard solution, like Grafana, for which we provide a dashboard.

The quickest route to providing customers with a true out of the box Monitoring solution is to close the loop and bundle Grafana and the default GitLab dashboard.

[**MVC**](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/3487)

#### Next - Using GitLab to monitor GitLab

GitLab includes an out-of-the-box project, where:
* Project visibility by default is internal
* Membership by default includes initial root user, more can be added manually
* [The GitLab server's metrics are automatically detected and displayed](https://gitlab.com/gitlab-org/gitlab-ce/issues/56673)

This would allow:
* A GitLab dashboard(s) to be auto-discovered and displayed

Next up, we could enable alerts to create issues and notify all administrators.

* Prometheus is configured to notify GitLab of alerts
* Membership automatically includes all users with instance Admin rights
* Issues created from alerts will mention `@all` to ping all admins: [1](https://gitlab.com/gitlab-org/gitlab-ce/issues/)55814, [2](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4100)
* Include runbooks in the repository, reference relevant book from each alert
* Updates to Deploy Board label matching to support Helm charts standards: [1](https://gitlab.com/gitlab-org/gitlab-ee/issues/5470)
  * This will enable Pod Logs and detailed pod views in k8s

This would allow:
* Alerts create issues and notify all administrators. These issues could then be acted on and closed, tracking resolution.
* Included runbooks help guide the administrator in troubleshooting and solving the problem based on what alert was triggered

#### Future

This project could be expanded in a number of different ways.

**Additional observability types**

GitLab also contains support for additional observability tools, which could be enabled as well. This includes error tracking, tracing, and logging. By leveraging these features, we could provide more complete visibility into the health of the GitLab instance.

* Error Tracking
  * Consider bundling Sentry
  * Enable Sentry error reporting and provide DSN's (automatic if bundled)
  * Enable Project Error Tracking integration for Sentry (automatic if bundled)
  * Sentry alerts will then also trigger issues and notifications to administrators
* Logging
  * Pod logs could be used for k8s deployments, until we have broader log support
  * Consider bundling Loki, and enable if Object Storage is enabled
  * Enable Logging in the project, configure for the Loki server (automatic if bundled)
* Tracing
  * Consider bundling Jaeger (seems impractical, requires Cassandra or ES)
  * Enable Jaeger tracing plugin for GitLab, and configure it for Jaeger instance (automatic if bundled)
  * Enable Tracing in the project, configure for the Jaeger server (automatic if bundled)
* Deploy status
  * Deploy boards will show the pod status

**Public dashboard**

Rather than build a specific feature for communicating the GitLab service health to end users, like the [I]nstance Health Dashboard](https://gitlab.com/gitlab-org/gitlab-ce/issues/50061), we could instead build on top of the upcoming [Status Page](https://gitlab.com/groups/gitlab-org/-/epics/208) features. 

The monitoring features above will automatically drive the data for the status page.

**Risks and Assumptions**

* This proposal is largely based on the premise that GitLab is more frequently degraded rather than down, and hard down situations could be prevented with proper proactive notifications.
* Those responsible for operating GitLab may already have a monitoring solution which spans multiple systems, and prefer to use that instead.

**Handling GitLab outages**

In the event GitLab is down and cannot be used, there are alternatives:
* A secondary GitLab instance could be used, and configured for the monitoring suite using the same services as the primary GitLab instance
  * Enter the Prometheus URL, Sentry URL/Token, Jaeger URL, and Loki service
  * We could also dogfood this model with our Ops instance of GitLab
* Grafana could be provisioned to view metrics and logs, while the existing Sentry and Jaeger interfaces could be leveraged for those services

## Maturity Plan
* [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/783)

## Competitive Landscape
The competitive landscape for this feature revolves primarily around the operational costs of running a self-managed Gitlab instance. If GitLab is unreliable or requires too time and effort to install/maintain, users will explore alternatives. 

Alternatives primarily fall into two categories:
* Other self-hosted application development platforms, like GitHub Enterprise or Bitbucket
* SaaS application development platforms, which offer reduced maintenance requirements

## Analyst Landscape
Not yet, but accepting merge requests to this document.

## Top Customer Success/Sales Issue(s)
Not yet, but accepting merge requests to this document.

## Top Customer Issue(s)
* [Prometheus SD for HA](https://gitlab.com/gitlab-org/gitlab-ce/issues/57379)
* [Built-in project for monitoring the instance](https://gitlab.com/gitlab-org/gitlab-ce/issues/56883)

## Top Internal Customer Issue(s)
* [Improving the instrumentation](https://gitlab.com/groups/gitlab-org/-/epics/826)

## Top Vision Item(s)
Not yet, but accepting merge requests to this document.

