---
layout: markdown_page
title: "GitLab Direction"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

Our vision is to replace disparate DevOps toolchains with a [single application](/handbook/product/single-application/)
that is pre-configured to work by default across the entire DevOps lifecycle.

We aim to make it faster and easier for groups of contributors to deliver value
to their users, and we achieve this by enabling:

 * Faster cycle time, driving an improved time to innovation
 * Easier workflows, driving increased collaboration and productivity

Our solution [plays well with others](/handbook/product/#plays-well-with-others), works for teams
of any size and composition and for any kind of project, and provides ongoing [actionable feedback](/handbook/product/#actionable-feedback)
for continuous improvement.

You can read more about the principles that guide our prioritization process
in our [product handbook](https://about.gitlab.com/handbook/product/#product-principles).

### Background and history

* [The Product Vision Backstory](/2018/10/09/gitlab-product-vision-back-story/)
* [The 2018 Product Vision](product-vision)
* [The 2019 and Beyond Product Vision](/2018/10/01/gitlab-product-vision/)

### DevOps stages

<!-- Image Map Generated by http://www.image-map.net/ -->
<!-- <img src="devops-loop-and-spans.png" usemap="#image-map" alt="DevOps Lifecycle"> -->
![DevOps Lifecycle](devops-loop-and-spans.png)

<!--
<map name="image-map">
    <area target="" alt="Manage" title="Manage" href="manage/" coords="813,32,1,1" shape="rect">
    <area target="" alt="Plan" title="Plan" href="plan/" coords="189,62,390,170" shape="rect">
    <area target="" alt="Create" title="Create" href="create/" coords="0,66,185,180" shape="rect">
    <area target="" alt="Verify" title="Verify" href="verify/" coords="1,187,200,309" shape="rect">
    <area target="" alt="Package" title="Package" href="package/" coords="209,183,410,303" shape="rect">
    <area target="" alt="Release" title="Release" href="release/" coords="401,63,628,179" shape="rect">
    <area target="" alt="Configure" title="Configure" href="configure/" coords="634,60,818,183" shape="rect">
    <area target="" alt="Monitor" title="Monitor" href="monitor" coords="611,190,813,309" shape="rect">
    <area target="" alt="Secure" title="Secure" href="secure" coords="819,378,3,343" shape="rect">
</map>
-->

<!-- ![Product Categories](categories.png) -->

DevOps is a broad space with a lot of complexity. To manage this within GitLab,
we break down the [DevOps lifecycle](https://en.wikipedia.org/wiki/DevOps_toolchain)
into a few different stages, each with its own strategy page you can review. We are investing in the following manner across each stage:

- [Planning spreadsheet](https://docs.google.com/spreadsheets/d/e/2PACX-1vSyBNOX8DEtgx78ZIRlRXvPRPTIAywSdT1If-z5wmFE8U4WxHkIERf5DLhaYoSP_URPnX-zzbNyE7AC/pubhtml) with PM analysis of competition, Headcount with stage revenue, Headcount by category
- [Product categories](https://about.gitlab.com/handbook/product/categories/)
- [Maturity page](/direction/maturity/)
- [Backend hiring priorities](https://gitlab.com/gitlab-com/people-ops/hiring-processes/blob/master/Engineering/Backend/README.md)
- [Rolling 4 Quarters GitLab Hiring Plan](https://docs.google.com/spreadsheets/d/12mNijMwA8hIG5h3zV5JJ0oxsVh6xzk6-si0eT7L9mvM/edit#gid=1361884179) (internal only)
- [Finding headcount outside of your stage](https://about.gitlab.com/handbook/product/#prioritize-global)

#### [Manage](/direction/manage)
Overall visibility and insight into what's happening within GitLab

#### [Plan](/direction/plan)
Planning capabilities to keep your team synchronized and moving in the right direction

#### [Create](/direction/create)
Create, view, and manage code and project data through powerful tooling

#### [CI/CD](/direction/cicd)

###### [Verify](/direction/verify)
Keep strict quality standards for production code with automatic testing and reporting

###### [Package](/direction/package)
Manages the packages you build and depend on in your software delivery process

###### [Release](/direction/release)
Continuous delivery and orchestration of your releases to your users

#### [Configure](/direction/configure)
Automation to configure your applications and infrastructure

#### [Monitor](/direction/monitor)

###### Application Performance Monitoring (APM)
Monitor system behavior to understand the impact of changes on your system

##### Debugging and Health
Triage, respond, resolve and prevent incidents to minimize downtime

#### [Secure](/direction/secure)
Security capabilities, directly integrated into your development lifecycle

#### [Defend](/direction/defend)
Defend your apps and infrastructure from security intrusions

### Enablement Groups

Supporting the stages in the DevOps lifecycle, there are additional Enablement
groups, again with their own direction pages:

#### [Geo](/direction/geo)
#### [Fulfillment](/direction/fulfillment)
#### [Ecosystem](/direction/ecosystem)

## Concepts

Product uses a few key concepts to talk about how we work:

![Product Strategy](productstrategy.png)

 * [Depth](#depth)
 * [Breadth](#breadth)
 * [Personas](#personas)
 * [Application Types](#application-types)

### Depth

We aim for market leadership in every category we compete in. We're already
there with:
* Source Code Management
* Continuous Integration

For FY20, and we'll build best-in-class, lovable features in our [four focus areas](product-vision/depth):
* [Project Management](https://gitlab.com/groups/gitlab-org/-/epics/666)
* Release Automation
* Application Security Testing
* [Value Stream Management](https://gitlab.com/groups/gitlab-org/-/epics/668)

Additionally, our FY20 goals include moving [these categories](/direction/complete-categories) to complete.

### Breadth

We wouldn't be true to our ambition if we stopped with our current product
categories. Across our existing DevOps stages we'll continue to rapidly expand
with [new categories](/direction/new-categories).

![New Categories](NewFY20.png)

For more information, check out our epics below:

Manage
- [Code Analytics](https://gitlab.com/groups/gitlab-org/-/epics/717)
- [Workflow Policies](https://gitlab.com/groups/gitlab-org/-/epics/720)

Plan
- [Requirements Mangement](https://gitlab.com/groups/gitlab-org/-/epics/670)
- [Quality Management](https://gitlab.com/groups/gitlab-org/-/epics/671)

Create
- [Design Management](https://gitlab.com/groups/gitlab-org/-/epics/370)
- [Live Coding](https://gitlab.com/groups/gitlab-org/-/epics/198)

Verify
- [System Testing](https://gitlab.com/groups/gitlab-org/-/epics/488)
- [Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/490)
- [Accessibility Testing](https://gitlab.com/groups/gitlab-org/-/epics/546)
- [Compatibility Testing](https://gitlab.com/groups/gitlab-org/-/epics/513)

Package
- [Rubygem Registry](https://gitlab.com/groups/gitlab-org/-/epics/455)
- [Linux Package Registry](https://gitlab.com/groups/gitlab-org/-/epics/185)
- [Helm Chart Registry](https://gitlab.com/groups/gitlab-org/-/epics/477)
- [Dependency Proxy](https://gitlab.com/groups/gitlab-org/-/epics/486)

Secure
- [Secret Detection](https://gitlab.com/groups/gitlab-org/-/epics/675)
- [IAST](https://gitlab.com/groups/gitlab-org/-/epics/344)
- [Fuzzing](https://gitlab.com/groups/gitlab-org/-/epics/818)

Release
- [Release Governance](https://about.gitlab.com/direction/release/release_governance/)

Configure
- [PaaS](https://gitlab.com/groups/gitlab-org/-/epics/111)
- [Chaos Engineering](https://gitlab.com/groups/gitlab-org/-/epics/381)
- [Cluster Cost Optimization](https://gitlab.com/groups/gitlab-org/-/epics/503)

Monitor
- [Synthetic Monitoring](https://gitlab.com/groups/gitlab-org/-/epics/168)
- [Incident Mangement](https://gitlab.com/groups/gitlab-org/-/epics/349)
- [Status Page](https://gitlab.com/groups/gitlab-org/-/epics/208)

Defend
- [Runtime Application Self Protection](https://gitlab.com/groups/gitlab-org/-/epics/443)
- [Web Application Firewall](https://gitlab.com/groups/gitlab-org/-/epics/795)
- [Threat Detection](https://gitlab.com/groups/gitlab-org/-/epics/744)
- [Behavior Analytics](https://gitlab.com/groups/gitlab-org/-/epics/748)
- [Vulnerability Management](https://gitlab.com/groups/gitlab-org/-/epics/797)
- [Data Loss Prevention](https://gitlab.com/groups/gitlab-org/-/epics/798)
- [Container Network Security](https://gitlab.com/groups/gitlab-org/-/epics/822)

We are additionally prioritizing getting [these categories](/direction/viable-categories) from Minimal to Viable.

### Personas

GitLab enables everyone to contribute. Our platform can be used by all people contributing to digital content.

![Full Scope](Fulldevops.png)

We've already built great workflows and dashboards for:
- Developers
- [Project Managers](/direction/project-managers)

We are investing in our relatively new roles:
- [Release Managers](/direction/personas/release-managers)
- Security
- Operations

We plan to expand to more roles in FY2020:
- [Designers](/direction/designers)
- Product Managers
- [Executives](/direction/personas/executives)

Other candidates:
- Marketing
- Legal
- QA
- Data Scientists

![Gitlab Devops](PersonasFY20.png)

### Application Types

Application types represent both different application types (web applications, mobile apps) and
architectures (monorepos, microservices). Gitlab continues to
deliver and replace the disparate DevOps toolchain for both static sites and
traditional web applications. These are two examples of application types. We're
actively investing in more robust support for new application types such as cloud native
and mobile apps, but we aren't done there. GitLab will expand to enable
collaboration on new application types. [More details](/handbook/product/application-types/).

In FY20, we're targeting:
- [Mobile apps](/direction/mobile) (including iOS builds on MacOS shared runners on GitLab.com)
- [Serverless functions](/direction/serverless)
- [Systems of microservices](/direction/microservices)
- [Monorepos of related services](/direction/monorepos)
- Versioned dependencies (e.g. rubygems)

Future application types:
- Data science
- ML/AI
- Security research
- Gaming (LFS / Monorepo with many binaries)

### Company Initiatives

We are also tracking big initatives for the entire company or for the
application at a holistic level that impact the product. These items have their
own planning and goals, and typically bridge several (and in some cases even
all) of the stages in the product. Some have formal [working groups](/company/team/structure/working-groups/).
There are other formal working groups that are not mentioned here as this list
focuses on efforts that Product is directly involved with.

- [Move to a single CE/EE Codebase](https://about.gitlab.com/company/team/structure/working-groups/single-codebase/)
- [GitLab.com costs](/company/team/structure/working-groups/gitlab-com-cost/)
- [Internationalization](https://gitlab.com/groups/gitlab-org/-/epics/650)
- [Elastic Search on GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/153)
- [GitLab.com using CD](https://gitlab.com/gitlab-org/release/framework/issues/1)
- GraphQL
- [Design system](https://about.gitlab.com/handbook/engineering/ux/pajamas-design-system/)
- [Dogfood microservices](https://gitlab.com/gitlab-com/Product/issues/168)

In addition, there are broad efforts to "level up" the Product org:
- [Embrace dogfooding](https://gitlab.com/gitlab-com/Product/issues/167)
- [Become more customer driven](https://gitlab.com/gitlab-com/Product/issues/142)
- [Become data-informed](https://gitlab.com/gitlab-com/Product/issues/166)
- [Become ROI and Revenue driven](https://gitlab.com/gitlab-com/Product/issues/165)
- [Shift ratio of breadth/depth more towards depth](https://gitlab.com/gitlab-com/Product/issues/174)
- [Improve latency without degrading bandwidth](https://gitlab.com/gitlab-com/Product/issues/176)

### Enterprise editions

GitLab comes in 4 editions:
* **Core**: This edition is aimed at solo developers or teams that
do not need advanced enterprise features. It contains a complete stack with all
the tools developers need to ship software.
* **Starter**: This edition contains features that are more
relevant for organizations that have more than 100 potential users. For example:
	* features for managers (reports, management tools at the group level,...),
	* features targeted at developers that have to work in multi-disciplinary
	teams (merge request approvals,...),
	* integrations with external tools.
* **Premium**: This edition contains features that are more
relevant for organizations that have more than 750 potential users. For example:
	* features for instance administrators
	* features for managers at the instance level (reporting, management tools,
	roles,...)
	* features to help teams that are spread around the world
	* features for people other than developers that help ship software (support,
	QA, legal,...)
* **Ultimate**: This edition contains
features that are more relevant for organizations that have more than 5000
potential users. For example:
	* compliances and certifications,
	* change management.

## Maturity

As we add new categories and stages to GitLab, some areas of the product will be deeper and more mature than others. We publish a
list of the categories, what we think their maturity levels are, and our plans to improve on our [maturity page](/direction/maturity/).

## Quarterly Objectives and Key Results (OKRs)

To make sure our goals are clearly defined and aligned throughout the
organization, we make use of OKR's (Objective Key Results). Our [quarterly Objectives and Key Results](/okrs)
are publicly viewable.

## Your contributions

GitLab's direction is determined by GitLab the company, and the code that is
sent by our [contributors](http://contributors.gitlab.com/). We continually
merge code to be released in the next version. Contributing is the best way to
get a feature you want included.

On [our issue tracker for CE][ce-issues] and [EE][ee-issues], many requests are
made for features and changes to GitLab. Issues with the [Accepting Merge
Requests] label are pre-approved as something we're willing to add to GitLab. Of
course, before any code is merged it still has to meet our [contribution
acceptance criteria].

[ce-issues]: https://gitlab.com/gitlab-org/gitlab-ce/issues
[ee-issues]: https://gitlab.com/gitlab-org/gitlab-ee/issues
[Accepting merge requests]: https://gitlab.com/gitlab-org/gitlab-ce/issues?state=opened&label_name=Accepting+merge+requests
[contribution acceptance criteria]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria

## How we plan releases

At GitLab, we strive to be [ambitious](/handbook/product/#be-ambitious),
maintain a strong sense of urgency, and set aspirational targets with every
release. The direction items we highlight in our [kickoff](/handbook/product/#kickoff-meeting)
are a reflection of this ambitious planning. When it comes to execution we aim
for
[velocity over predictability](/handbook/engineering/#velocity-over-predictability).
This way we optimize our planning time to focus on the top of the queue and
deliver things fast. We schedule 100% of what we can accomplish based on past [throughput](/handbook/engineering/management/throughput/)
and availability factors (vacation, contribute, etc.).

See our [product handbook on how we prioritize](/handbook/product/#prioritization).

## Previous releases

On our [releases page](/releases/) you can find an overview of the most
important features of recent releases and links to the blog posts for each
release.

## Upcoming releases

GitLab releases a new version [every single month on the 22nd](/2015/12/07/why-we-shift-objectives-and-not-release-dates-at-gitlab). You can find the
major planned features for upcoming releases on our [upcoming releases page](/upcoming-releases/).

Note that we often move things around, do things that are not listed, and cancel
things that _are_ listed.

## Moonshots

Moonshots are big hairy audacious goals that may take a long time to deliver.

<%= wishlist["moonshots"] %>

## Paid tiers

### Starter

Starter features are available to anyone with an Enterprise Edition subscription (Starter, Premium, Ultimate).

<%= wishlist["GitLab Starter"] %>

### Premium

Premium features are only available to Premium (and Ultimate) subscribers.

<%= wishlist["GitLab Premium"] %>

### Ultimate

Ultimate is for organizations that have a need to build secure, compliant
software and that want to gain visibility of - and be able to influence - their
entire organization from a high level. Ultimate features are only be available
to Ultimate subscribers.

<%= wishlist["GitLab Ultimate"] %>
